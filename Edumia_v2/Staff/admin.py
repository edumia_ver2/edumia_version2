from django.contrib import admin
from Staff.models import StaffProfile,Departments,Designation,StaffCategory,Grade
from users.models import CustomUser
from django.contrib.auth.models import Group
from .forms import GroupAdminForm

admin.site.register(Departments)
admin.site.register(Designation)
admin.site.register(StaffCategory)
admin.site.register(Grade)

class CustomUserInline(admin.TabularInline):
    model = CustomUser

@admin.register(StaffProfile)
class StaffProfileAdmin(admin.ModelAdmin):
     fields = ['employee_id',
         ('first_name','last_name'),
         ('email','contact_number'),
          'gender','blood_group','date_of_birth',
          ('department','category','designation'),'is_teacher',
          'qualification','date_joined','grade','marital_status','no_of_children','father_name','mother_name',
          'experience','user'
              ]


# Unregister the original Group admin.
admin.site.unregister(Group)

# Create a new Group admin.
class GroupAdmin(admin.ModelAdmin):
    # Use our custom form.
    form = GroupAdminForm
    # Filter permissions horizontal as well.
    filter_horizontal = ['permissions']

# Register the new Group ModelAdmin.
admin.site.register(Group, GroupAdmin)