from django.urls import path
from .views import *
from . import views


urlpatterns = [
    path('',views.staff_dashboard,name='staff_dashboard'),
    path('add_staff',views.add_staff,name='add_staff'),
    path('staffdetail/<int:pk>/',views.StaffDetailView.as_view(),name='staffdetail'),
]