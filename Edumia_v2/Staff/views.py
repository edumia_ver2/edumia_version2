from django.shortcuts import render,redirect,HttpResponse
from django.views import generic

from users.forms import CustomUserCreationForm,CustomUserChangeForm
from Staff.models import StaffProfile
from .forms import *
import requests
from rest_framework.authtoken.models import Token
from Edumia_v2.settings import URL
def staff_dashboard(request):
    staffprofile = StaffProfile.objects.all()
    return render(request,'Staff/staff_dashboard.html',{'staff_data':staffprofile})
def add_staff(request):
    if request.method == 'POST':
        staff_form = StaffCreationForm(prefix="staff",data=request.POST)
        profile_form = ProfileForm(prefix='profile',data=request.POST)
        if staff_form.is_valid() and profile_form.is_valid():
            staff=staff_form.save()
            profile = profile_form.save(commit=False)
            profile.user=staff
            profile.save()
            return HttpResponse('added')
    else:
        staff_form = StaffCreationForm(prefix='staff')
        profile_form = ProfileForm(prefix='profile')
    return render(request,'Staff/create_staff.html',{'staff_form':staff_form,'profile_form':profile_form})

class StaffDetailView(generic.DetailView):
    model = StaffProfile