# users/urls.py
from django.urls import path
from .views import SignUpView
from .import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('password',views.change_password,name='change_password'),

]