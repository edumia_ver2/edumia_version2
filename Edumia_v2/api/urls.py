from rest_framework import routers
from .views import *
from django.urls import path,include
from api import views



#api
router = routers.DefaultRouter()
router.register(r'category_list',StudentCategoryViewSet)
router.register(r'studentsprofile',StudentProfile,basename='studentsprofile')
#router.register(r'klass_students',KlassStudentList,basename='klass_students')
#staff module
router.register(r'staffprofile',StaffProfileViewSet,basename='staffprofile')

urlpatterns = [
    path('klass_students',views.KlassStudentList.as_view()),
    path('search',views.SearchStudent.as_view()),
    path('staffsearch',views.SearchStaff.as_view()),

    ]+router.urls