from rest_framework import serializers
from students import models
from Staff.models import StaffProfile, StaffCategory, Departments, Designation, Grade
from phonenumber_field.serializerfields import PhoneNumberField


# student module related serializers start here
class StudentsProfileSerializer(serializers.HyperlinkedModelSerializer):
    phone_number = serializers.CharField(
        validators=PhoneNumberField().validators, )
    mother_phone_number = serializers.CharField(
        validators=PhoneNumberField().validators, )
    user = serializers.CharField(source='user.number')
    klass = serializers.CharField(source='klass.name')
    division = serializers.CharField(source='division.name')
    category = serializers.CharField(source='category.name')

    class Meta:
        model = models.StudentsProfile
        fields = ['url', 'id', 'status', 'admission_number', 'admission_date', 'klass', 'division',
                  'first_name', 'last_name',
                  'birthday', 'gender',
                  'birth_place', 'nationality',
                  'mothertounge', 'category',
                  'religion', 'caste', 'caste_category',
                  'address_line1', 'address_line2',
                  'city', 'state',
                  'pin_code', 'country',
                  'father_name', 'phone_number',
                  'mother_name', 'mother_phone_number',
                  'aadhaar_Number', 'email',
                  'height', 'weight', 'blood_group',
                  'vaccination_details', 'user', ]


class StudentCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StudentCategory


# klass module seril

class KlassSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Klass
        fields = '__all__'


class DivisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Division
        fields = '__all__'


# staff module
class StaffDepartmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departments
        fields = '__all__'


class StaffCategorySerialiizer(serializers.ModelSerializer):
    class Meta:
        model = StaffCategory
        fields = '__all__'


class StaffDesignationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Designation
        fields = '__all__'


class StaffGradeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Grade
        fields = '__all__'


class StaffProfileSerializer(serializers.ModelSerializer):
    department = serializers.CharField(source='department.name')
    category = serializers.CharField(source='category.name')
    designation = serializers.CharField(source='designation.name')
    grade = serializers.CharField(source='grade.grade')
    user = serializers.CharField(source='user.number')

    class Meta:
        model = StaffProfile
        fields = [
            'employee_id',
            'first_name',
            'last_name',
            'email',
            'contact_number',
            'gender',
            'date_of_birth',
            'blood_group',
            'department',
            'is_teacher',
            'category',
            'designation',
            'qualification',
            'experience',
            'date_joined',
            'grade',
            'marital_status',
            'no_of_children',
            'father_name',
            'mother_name', 'user',
        ]
