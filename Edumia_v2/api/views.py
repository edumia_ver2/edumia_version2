import requests
from .serializers import StudentsProfileSerializer, StudentCategorySerializer, KlassSerializer, DivisionSerializer
from .serializers import *
from rest_framework import viewsets
from students.models import StudentsProfile, StudentCategory, Klass, Division
from Staff.models import StaffProfile, StaffCategory, Departments, Designation, Grade
from rest_framework.authentication import SessionAuthentication, BasicAuthentication, TokenAuthentication
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated, IsAdminUser
from api.permissions import IsTeacher, IsUser, IsSuperUser
from django.shortcuts import HttpResponse
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters


class StudentProfile(viewsets.ModelViewSet):
    serializer_class = StudentsProfileSerializer
    queryset = StudentsProfile.objects.all()
    authentication_classes = [TokenAuthentication]

    permission_classes = [IsAdminUser | IsUser]

    def get_queryset(self):
        """
        This view should return profile
        for the currently authenticated user.
        """
        if self.request.user.is_staff:
            return StudentsProfile.objects.all()
        else:
            user = self.request.user.id
            return StudentsProfile.objects.filter(user=user)


class StudentCategoryViewSet(viewsets.ModelViewSet):
    queryset = StudentCategory.objects.all()
    serializer_class = StudentCategorySerializer


class KlassStudentList(generics.ListAPIView):
    """return all students from a klass-division"""
    serializer_class = StudentsProfileSerializer
    # permission_classes = [IsAuthenticated]
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdminUser | IsTeacher]
    queryset = StudentsProfile.objects.all()
    # def get_queryset(self):
    #     """
    #             Optionally restricts the returned students to a given klass ,
    #             by filtering against a `klass` query parameter in the URL.
    #             """
    #     queryset = StudentsProfile.objects.all()
    #     klass = self.request.query_params.get('klass',None)
    #     if klass is not None:
    #         queryset = queryset.filter(klass=klass)
    #     return queryset
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['klass', 'division']


class SearchStudent(generics.ListAPIView):
    queryset = StudentsProfile.objects.all()
    serializer_class = StudentsProfileSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdminUser | IsTeacher]
    filter_backends = [filters.SearchFilter]
    search_fields = ['admission_number', 'admission_date', 'klass__name', 'division__name',
                     'first_name', 'last_name',
                     'birthday', 'gender',
                     'birth_place', 'nationality',
                     'mothertounge', 'category__name',
                     'religion', 'caste', 'caste_category',
                     'address_line1', 'address_line2',
                     'city', 'state',
                     'pin_code', 'country',
                     'father_name', 'phone_number',
                     'mother_name', 'mother_phone_number',
                     'aadhaar_Number', 'email',
                     'height', 'weight', 'blood_group']


# klass module views
class ClassViewSet(viewsets.ModelViewSet):
    queryset = Klass.objects.all()
    serializer_class = KlassSerializer


class DivisionViewSet(viewsets.ModelViewSet):
    queryset = Division.objects.all()
    serializer_class = DivisionSerializer


# staff module views
class StaffProfileViewSet(viewsets.ModelViewSet):
    serializer_class = StaffProfileSerializer
    queryset = StaffProfile.objects.all()
    authentication_classes = [TokenAuthentication]

    permission_classes = [IsAdminUser | IsTeacher]

    def get_queryset(self):
        """
        This view should return profile
        for the currently authenticated user.
        """
        if self.request.user.is_staff:
            return StaffProfile.objects.all()
        else:
            user = self.request.user.id
            return StaffProfile.objects.filter(user=user)


class SearchStaff(generics.ListAPIView):
    queryset = StaffProfile.objects.all()
    serializer_class = StaffProfileSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAdminUser | IsTeacher]
    filter_backends = [filters.SearchFilter]
    search_fields = [
        'employee_id',
        'first_name',
        'last_name',
        'email',
        'contact_number',
        'gender',
        'date_of_birth',
        'blood_group',
        'department__department',
        'is_teacher',
        'category__staffcategory',
        'designation__designation',
        'qualification',
        'experience',
        'date_joined',
        'grade__grade',
        'marital_status',
        'father_name',
        'mother_name', ]
