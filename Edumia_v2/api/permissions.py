from rest_framework import permissions


class IsTeacher(permissions.BasePermission):
    def has_permission(self,request,view):
        if request.user.groups.filter(name='Teacher'):
            return True
        return False


class IsUser(permissions.BasePermission):

    def has_object_permission(self, request, view, obj):
        if request.user:
            if request.user.is_superuser:
                return True
            else:
                return obj == request.user
        else:
            return False

class IsSuperUser(permissions.BasePermission):

    def has_permission(self, request, view):
        return request.user and request.user.is_superuser
