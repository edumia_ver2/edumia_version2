from django.db import models

# Create your models here.
class Klass(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)


    def __str__(self):
        return self.name


class Division(models.Model):
    klass = models.ForeignKey(Klass, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    klass_teacher = models.OneToOneField('Staff.StaffProfile',on_delete=models.CASCADE)


    def __str__(self):
        return self.name

class SubjectTeacher(models.Model):
    division = models.ForeignKey('Division',on_delete=models.CASCADE)
    subject = models.ForeignKey('subject.Subject',on_delete=models.CASCADE)
    teacher = models.ForeignKey('Staff.StaffProfile',on_delete=models.CASCADE)

    def __str__(self):
        return '{0} {1},{2}'.format(self.subject,self.division,self.teacher)