from django.shortcuts import render
from klass.forms import DivisionForm
from django.views.generic import CreateView
from klass.models import Division
from django.urls import reverse_lazy
from django.http import HttpResponse
# Create your views here.

class AddDivision(CreateView):
    model = Division
    form_class = DivisionForm
    success_url = reverse_lazy("success")

def success(request):
    return HttpResponse('added division')


