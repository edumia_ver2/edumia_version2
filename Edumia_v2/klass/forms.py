from django import forms
from klass.models import Division
from Staff.models import StaffProfile


class DivisionForm(forms.ModelForm):

    class Meta:
        model = Division
        fields = ['klass',
                  'name',
                  'klass_teacher']
    def __init__(self,*args,**kwargs):
        super().__init__(*args,**kwargs)
        self.fields['klass_teacher'].queryset = StaffProfile.objects.filter(is_teacher=True)

class KlassDivisionForm(forms.ModelForm):
    division = forms.ModelChoiceField(queryset=Division.objects.all().order_by('name'))
    class Meta:
        model = Division
        fields = ['klass']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['division'].queryset = Division.objects.none()