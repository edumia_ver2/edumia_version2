from django.contrib import admin
from students.models import StudentsProfile, StudentCategory, Klass, Division,StudentAttendance,StudentDetails

# Register your models here.



class StudentProfileAdmin(admin.ModelAdmin):
    fields = [
        ('admission_number', 'admission_date'), ('klass', 'division'),
        ('first_name', 'last_name'),
        'birthday', 'gender',
        ('birth_place', 'nationality'),
        'mothertounge', 'category',
        ('religion', 'caste', 'caste_category'),
        ('address_line1', 'address_line2'),
        ('city', 'state'),
        ('pin_code', 'country'),
        ('father_name', 'phone_number'),
        ('mother_name', 'mother_phone_number'),
        ('aadhaar_Number', 'email'),
        ('height', 'weight'), 'blood_group',
        'vaccination_details','user'
    ]


admin.site.register(StudentsProfile, StudentProfileAdmin)
admin.site.register(StudentCategory)
admin.site.register(Klass)
admin.site.register(Division)
admin.site.register(StudentAttendance)
admin.site.register(StudentDetails)
