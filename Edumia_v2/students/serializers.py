from rest_framework import serializers
from students import models


class StudentsProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.StudentsProfile
        fields = (
            'admission_number',
            'admission_date',
            'klass',
            'division',
            'first_name',
            'last_name',
            'birthday',
            'gender',
            'birth_place',
            'nationality',
            'mothertounge',
            'category',
            'religion',
            'caste',
            'caste',
            'category',
            'address_line1',
            'address_line2',
            'city', 'state',
            'pin_code',
        )
