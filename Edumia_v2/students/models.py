from django.db import models
from django.urls import reverse
from django_countries.fields import CountryField
from django.conf import settings
from klass.models import Klass,Division
from phonenumber_field.modelfields import PhoneNumberField
from django.utils import timezone


# Create your models here.
class StudentsProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    admission_number = models.IntegerField(null=False, unique=True)
    admission_date = models.DateField()
    status_choice = (("0", "Pending"),
                     ("1", "Approve"),
                     ("2", "Deny"))
    status = models.CharField(max_length=1, default=0, choices=status_choice)
    klass = models.ForeignKey('klass.Klass', on_delete=models.CASCADE)
    division = models.ForeignKey("klass.Division", on_delete=models.CASCADE)

    # Personnel Details
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(max_length=100,
                                  help_text="Enter First Name")
    last_name = models.CharField(max_length=100,
                                 help_text="Enter Last Name")
    birthday = models.DateField()

    gender_choice = (("M", "Male"),
                     ("F", "Female"),
                     ("O", "Other"))
    gender = models.CharField(max_length=1, choices=gender_choice)
    birth_place = models.CharField(max_length=100, null=True, blank=True)
    nationality = CountryField()
    mothertounge = models.CharField(max_length=100, null=True, blank=True)
    category = models.ForeignKey("StudentCategory", on_delete=models.CASCADE)

    religion = models.CharField(max_length=100, null=True, blank=True)
    caste = models.CharField(max_length=100, null=True, blank=True)
    caste_category = models.CharField(max_length=100, null=True, blank=True)

    # Contact Details

    address_line1 = models.CharField(max_length=250)
    address_line2 = models.CharField(max_length=250)
    city = models.CharField(max_length=100)
    state = models.CharField(max_length=100)
    pin_code = models.CharField(max_length=100)
    country = CountryField()
    father_name = models.CharField(max_length=100)
    phone_number = PhoneNumberField()
    mother_name = models.CharField(max_length=100, null=True, blank=True)
    mother_phone_number = PhoneNumberField()
    aadhaar_Number = models.CharField(max_length=100, null=True, blank=True)
    email = models.EmailField(max_length=254, null=True, blank=True)

    # Health Report

    height = models.CharField(max_length=100, null=True, blank=True)
    weight = models.CharField(max_length=100, null=True, blank=True)
    blood_choice = (("A+", "A Positive"),
                    ("A-", "A Negative"),
                    ("B+", "B Positive"),
                    ("B-", "B Negative"),
                    ("O+", "O Positive"),
                    ("O-", "O Negative"),
                    ("AB+", "AB Positive"),
                    ("AB-", "AB Negative"))
    blood_group = models.CharField(max_length=3, choices=blood_choice)
    vaccination_details = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.first_name

    def session_data(self):
        session = {
            'admission_date': self.admission_date,
            'klass': self.klass,
            'category': self.category,
            'phone_number': self.phone_number
        }
        return session
    def get_absolute_url(self):
        return reverse('profile_page',args[str(self.id)])


class StudentCategory(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name





class StudentDetails(models.Model):
    student = models.ForeignKey(StudentsProfile, on_delete=models.CASCADE)
    klass = models.ForeignKey(Klass, on_delete=models.CASCADE)
    division = models.ForeignKey(Division, on_delete=models.CASCADE)


class StudentAttendance(models.Model):
    student = models.ForeignKey(StudentDetails, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    academic_year = models.ForeignKey('users.SessionCalendar',on_delete=models.CASCADE)
    timetable = models.CharField(max_length=32)
    session_choice = (("1","Present"),
                      ("0","Absent"))
    session1 = models.CharField(max_length=1, choices=session_choice)
    session2 = models.CharField(max_length=1, choices=session_choice)



    # The value in this field is derived from the first two.
    # Updated every time the model instance is saved.
    daily_attendance = models.PositiveIntegerField(
        default=0,  # This value will be overwritten during save()
        editable=False,  # Hides this field in the admin interface.
    )

    def save(self, *args, **kwargs):
        # calculate sum before saving.
        self.daily_attendance = self.calculate_sum()
        super(StudentAttendance, self).save(*args, **kwargs)

    def calculate_sum(self):
        """ Calculate a numeric value for the model instance. """
        try:
            return (int(self.session1) + int(self.session2))
        except KeyError:
            # Value_a or value_b is not in the VALUES dictionary.
            # Do something to handle this exception.
            # Just returning the value 0 will avoid crashes, but could
            # also hide some underlying problem with your data.
            return 0

