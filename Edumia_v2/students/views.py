from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from students.models import StudentsProfile,Division,StudentCategory
from django.http import HttpResponse
from django.shortcuts import redirect
from .forms import studentprofileform
from klass.forms import KlassDivisionForm
from django.views.generic import CreateView,View
from django.urls import reverse_lazy
import requests
from rest_framework.authtoken.models import Token
from Edumia_v2.settings import URL
from api.serializers import StudentsProfileSerializer
# Create your views here.
def pending(request):
    pending_students=all_students(request)
    return render(request, 'students/students_profile.html', {'sp': pending_students})

def status_approvel(request):
    if request.method == 'GET':
        student_id = request.GET.get('id')
        student = StudentsProfile.objects.get(id=student_id)
        student.status = '1'
        student.save()
        pending_students = StudentsProfile.objects.all()
        return render(request, 'students/students_profile.html', {'sp': pending_students})

def status_denial(request):
    student_id = request.GET.get('id')
    student = StudentsProfile.objects.get(id=student_id)
    student.status = '2'
    student.save()
    pending_students = StudentsProfile.objects.all()
    return render(request, 'students/students_profile.html', {'sp': pending_students})

@login_required
def studentdashboard(request):
    token, created = Token.objects.get_or_create(user=request.user)
    return render(request,'students/students_dashboard.html',{'token':token})

# class AddStudent(CreateView):
#     model = StudentsProfile
#     form_class = studentprofileform
#     success_url = reverse_lazy('success')
def success(request):
    return HttpResponse('student added successfully')

def AddStudent(request):
    form = studentprofileform
    return render(request,'students/studentsprofile_form.html',{'form':form})
def get_divisions(request):
    klass_id = request.GET.get('klass')
    divisions = Division.objects.filter(klass_id=klass_id).order_by('name')
    return render(request, 'students/drop_down_divisions.html', {'divisions': divisions})

def all_students(request):
    token,created = Token.objects.get_or_create(user=request.user)
    header = {'Authorization':f'Token {token}'}
    response = requests.get((URL+'api/v1/studentsprofile/'),headers=header)
    students = response.json()
    return render(request,'students/allstudents.html',{'data':students})
    #return students


def profile(request):
    token, created = Token.objects.get_or_create(user=request.user)
    header = {'Authorization': f'Token {token}'}
    response = requests.get((URL + 'api/v1/studentsprofile/'), headers=header)
    students = response.json()
    return render(request, 'students/profile.html', {'data': students})

def get_profile(request):
    token, created = Token.objects.get_or_create(user=request.user)
    header = {'Authorization': f'Token {token}'}
    response = requests.get((URL + 'api/v1/studentsprofile/'), headers=header)
    students = response.json()
    return render(request, 'students/profile.html', {'data': students})

def student_phone_numbers(request):
    "page to select class and division to get contact number"
    form = KlassDivisionForm
    return render(request, 'students/student_phone_numbers.html', {'form': form})
def contact_numbers(request):
    "redirected from student_phone_numbers page with class and division filtered contact number list"
    klass = request.GET.get('klass')
    division = request.GET.get('division')
    token, created = Token.objects.get_or_create(user=request.user)
    header = {'Authorization': f'Token {token}'}
    api_end = 'api/v1/klass_students?'
    url="{URL}{api_end}klass={klass}&division={div}".format(URL=URL,api_end=api_end,klass=klass,div=division)
    response = requests.get(url, headers=header)
    students = response.json()
    return render(request, 'students/student_phone_numbers.html', {'data': students})

def student_info(request):
    "get all info from all students of a class-division page to select class and division to get contact number"
    form = KlassDivisionForm
    return render(request, 'students/student_info.html', {'form': form})





