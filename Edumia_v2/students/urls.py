from django.urls import path,include
from students import views


urlpatterns = [path('pending',views.pending,name="pending"),
               path('status_approval',views.status_approvel,name="approvel"),
               path('status_denial',views.status_denial,name="denial"),
               path('',views.studentdashboard,name="studentdashboard"),
               path('add-student/', views.AddStudent, name='add-student'),
               path('reverse',views.success,name="success"),
               path('ajax/get_divisions/', views.get_divisions, name='ajax_get_divisions'),
               path('all_students',views.all_students,name = 'all_students'),
               path('profile',views.profile,name='profile'),
               path('student_phone_numbers',views.student_phone_numbers,name='student_phone_numbers'),
               path('contact_numbers',views.contact_numbers,name='contact_numbers'),
               path('student_info',views.student_info,name='student_info'),

               ]